RECONNECT_TIMES = 3    # 尝试重连次数
DEBUG = False          # 开启debug
LOGGER = None          # 日志器
DB_TYPE = 'mysql'      # 数据库的种类
EXT = ''               # 扩展信息
SSLKEY_PATH = ''       # ssl路径
